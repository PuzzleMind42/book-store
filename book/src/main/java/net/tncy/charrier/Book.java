package net.tncy.charrier;

import net.tncy.validator.constraints.books.ISBN;

public class Book
{
    private String author;
    private String title;
    @ISBN
    private String ISBN;

    public Book(String a, String t, String isbn)
    {
        this.author = a;
        this.ISBN = isbn;
        this.title = t;
    }
}
